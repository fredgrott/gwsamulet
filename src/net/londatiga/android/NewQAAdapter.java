package net.londatiga.android;

import org.bitbucket.fredgrott.gwsamulet.R;

import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;

import android.content.Context;

import android.widget.BaseAdapter;
import android.widget.TextView;

// TODO: Auto-generated Javadoc
/**
 * The Class NewQAAdapter.
 */
public class NewQAAdapter extends BaseAdapter {
	
	/** The m inflater. */
	private LayoutInflater mInflater;
	
	/** The data. */
	private String[] data;
	
	/**
	 * Instantiates a new new qa adapter.
	 *
	 * @param context the context
	 */
	public NewQAAdapter(Context context) { 
		mInflater = LayoutInflater.from(context);
	}

	/**
	 * Sets the data.
	 *
	 * @param data the new data
	 */
	public void setData(String[] data) {
		this.data = data;
	}
	
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		return data.length;
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int item) {
		return data[item];
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int position) {
		return position;
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if (convertView == null) {
			convertView	= mInflater.inflate(R.layout.list, null);
			
			holder 		= new ViewHolder();
			
			holder.mTitleText	= (TextView) convertView.findViewById(R.id.t_name);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.mTitleText.setText(data[position]);
		
		return convertView;
	}

	/**
	 * The Class ViewHolder.
	 */
	static class ViewHolder {
		
		/** The m title text. */
		TextView mTitleText;
	}
}