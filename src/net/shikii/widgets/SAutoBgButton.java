package net.shikii.widgets;

import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.AttributeSet;
import android.widget.Button;

// TODO: Auto-generated Javadoc
/**
 * Applies a pressed state color filter or disabled state alpha for the button's background
 * drawable.
 * 
 * @author shiki
 */
public class SAutoBgButton extends Button {

  /**
   * Instantiates a new s auto bg button.
   *
   * @param context the context
   */
  public SAutoBgButton(Context context) {
    super(context);
  }

  /**
   * Instantiates a new s auto bg button.
   *
   * @param context the context
   * @param attrs the attrs
   */
  public SAutoBgButton(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  /**
   * Instantiates a new s auto bg button.
   *
   * @param context the context
   * @param attrs the attrs
   * @param defStyle the def style
   */
  public SAutoBgButton(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
  }

  /* (non-Javadoc)
   * @see android.view.View#setBackgroundDrawable(android.graphics.drawable.Drawable)
   */
  @SuppressWarnings("deprecation")
@Override
  public void setBackgroundDrawable(Drawable d) {
    // Replace the original background drawable (e.g. image) with a LayerDrawable that
    // contains the original drawable.
    SAutoBgButtonBackgroundDrawable layer = new SAutoBgButtonBackgroundDrawable(d);
    super.setBackgroundDrawable(layer);
  }

  /**
   * The stateful LayerDrawable used by this button. 
   */
  protected class SAutoBgButtonBackgroundDrawable extends LayerDrawable {

    // The color filter to apply when the button is pressed
    /** The _pressed filter. */
    protected ColorFilter _pressedFilter = new LightingColorFilter(Color.LTGRAY, 1);
    // Alpha value when the button is disabled
    /** The _disabled alpha. */
    protected int _disabledAlpha = 100;

    /**
     * Instantiates a new s auto bg button background drawable.
     *
     * @param d the d
     */
    public SAutoBgButtonBackgroundDrawable(Drawable d) {
      super(new Drawable[] { d });
    }

    /**
     * On state change.
     *
     * @param states the states
     * @return true, if successful
     * @see android.graphics.drawable.LayerDrawable#onStateChange(int[])
     */
    @Override
    protected boolean onStateChange(int[] states) {
      boolean enabled = false;
      boolean pressed = false;

      for (int state : states) {
        if (state == android.R.attr.state_enabled)
          enabled = true;
        else if (state == android.R.attr.state_pressed)
          pressed = true;
      }

      mutate();
      if (enabled && pressed) {
        setColorFilter(_pressedFilter);
      } else if (!enabled) {
        setColorFilter(null);
        setAlpha(_disabledAlpha);
      } else {
        setColorFilter(null);
      }

      invalidateSelf();

      return super.onStateChange(states);
    }

    /**
     * Checks if is stateful.
     *
     * @return true, if is stateful
     * @see android.graphics.drawable.LayerDrawable#isStateful()
     */
    @Override
    public boolean isStateful() {
      return true;
    }
  }

}
