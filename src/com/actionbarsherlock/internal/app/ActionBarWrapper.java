package com.actionbarsherlock.internal.app;

import java.util.HashSet;
import java.util.Set;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.SpinnerAdapter;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;

// TODO: Auto-generated Javadoc
/**
 * The Class ActionBarWrapper.
 */
public class ActionBarWrapper extends ActionBar implements android.app.ActionBar.OnNavigationListener, android.app.ActionBar.OnMenuVisibilityListener {
    
    /** The m activity. */
    private final Activity mActivity;
    
    /** The m action bar. */
    private final android.app.ActionBar mActionBar;
    
    /** The m navigation listener. */
    private ActionBar.OnNavigationListener mNavigationListener;
    
    /** The m menu visibility listeners. */
    private Set<OnMenuVisibilityListener> mMenuVisibilityListeners = new HashSet<OnMenuVisibilityListener>(1);
    
    /** The m fragment transaction. */
    private FragmentTransaction mFragmentTransaction;


    /**
     * Instantiates a new action bar wrapper.
     *
     * @param activity the activity
     */
    public ActionBarWrapper(Activity activity) {
        mActivity = activity;
        mActionBar = activity.getActionBar();
        if (mActionBar != null) {
            mActionBar.addOnMenuVisibilityListener(this);
        }
    }


    /**
     * Sets the home button enabled.
     *
     * @param enabled the new home button enabled
     * @see com.actionbarsherlock.app.ActionBar#setHomeButtonEnabled(boolean)
     */
    @Override
    public void setHomeButtonEnabled(boolean enabled) {
        mActionBar.setHomeButtonEnabled(enabled);
    }

    /**
     * Gets the themed context.
     *
     * @return the themed context
     * @see com.actionbarsherlock.app.ActionBar#getThemedContext()
     */
    @Override
    public Context getThemedContext() {
        return mActionBar.getThemedContext();
    }

    /**
     * Sets the custom view.
     *
     * @param view the new custom view
     * @see com.actionbarsherlock.app.ActionBar#setCustomView(android.view.View)
     */
    @Override
    public void setCustomView(View view) {
        mActionBar.setCustomView(view);
    }

    /**
     * Sets the custom view.
     *
     * @param view the view
     * @param layoutParams the layout params
     * @see com.actionbarsherlock.app.ActionBar#setCustomView(android.view.View, com.actionbarsherlock.app.ActionBar.LayoutParams)
     */
    @Override
    public void setCustomView(View view, LayoutParams layoutParams) {
        android.app.ActionBar.LayoutParams lp = new android.app.ActionBar.LayoutParams(layoutParams);
        lp.gravity = layoutParams.gravity;
        lp.bottomMargin = layoutParams.bottomMargin;
        lp.topMargin = layoutParams.topMargin;
        lp.leftMargin = layoutParams.leftMargin;
        lp.rightMargin = layoutParams.rightMargin;
        mActionBar.setCustomView(view, lp);
    }

    /**
     * Sets the custom view.
     *
     * @param resId the new custom view
     * @see com.actionbarsherlock.app.ActionBar#setCustomView(int)
     */
    @Override
    public void setCustomView(int resId) {
        mActionBar.setCustomView(resId);
    }

    /**
     * Sets the icon.
     *
     * @param resId the new icon
     * @see com.actionbarsherlock.app.ActionBar#setIcon(int)
     */
    @Override
    public void setIcon(int resId) {
        mActionBar.setIcon(resId);
    }

    /**
     * Sets the icon.
     *
     * @param icon the new icon
     * @see com.actionbarsherlock.app.ActionBar#setIcon(android.graphics.drawable.Drawable)
     */
    @Override
    public void setIcon(Drawable icon) {
        mActionBar.setIcon(icon);
    }

    /**
     * Sets the logo.
     *
     * @param resId the new logo
     * @see com.actionbarsherlock.app.ActionBar#setLogo(int)
     */
    @Override
    public void setLogo(int resId) {
        mActionBar.setLogo(resId);
    }

    /**
     * Sets the logo.
     *
     * @param logo the new logo
     * @see com.actionbarsherlock.app.ActionBar#setLogo(android.graphics.drawable.Drawable)
     */
    @Override
    public void setLogo(Drawable logo) {
        mActionBar.setLogo(logo);
    }

    /**
     * Sets the list navigation callbacks.
     *
     * @param adapter the adapter
     * @param callback the callback
     * @see com.actionbarsherlock.app.ActionBar#setListNavigationCallbacks(android.widget.SpinnerAdapter, com.actionbarsherlock.app.ActionBar.OnNavigationListener)
     */
    @Override
    public void setListNavigationCallbacks(SpinnerAdapter adapter, OnNavigationListener callback) {
        mNavigationListener = callback;
        mActionBar.setListNavigationCallbacks(adapter, (callback != null) ? this : null);
    }

    /**
     * On navigation item selected.
     *
     * @param itemPosition the item position
     * @param itemId the item id
     * @return true, if successful
     * @see android.app.ActionBar.OnNavigationListener#onNavigationItemSelected(int, long)
     */
    @Override
    public boolean onNavigationItemSelected(int itemPosition, long itemId) {
        //This should never be a NullPointerException since we only set
        //ourselves as the listener when the callback is not null.
        return mNavigationListener.onNavigationItemSelected(itemPosition, itemId);
    }

    /**
     * Sets the selected navigation item.
     *
     * @param position the new selected navigation item
     * @see com.actionbarsherlock.app.ActionBar#setSelectedNavigationItem(int)
     */
    @Override
    public void setSelectedNavigationItem(int position) {
        mActionBar.setSelectedNavigationItem(position);
    }

    /**
     * Gets the selected navigation index.
     *
     * @return the selected navigation index
     * @see com.actionbarsherlock.app.ActionBar#getSelectedNavigationIndex()
     */
    @Override
    public int getSelectedNavigationIndex() {
        return mActionBar.getSelectedNavigationIndex();
    }

    /**
     * Gets the navigation item count.
     *
     * @return the navigation item count
     * @see com.actionbarsherlock.app.ActionBar#getNavigationItemCount()
     */
    @Override
    public int getNavigationItemCount() {
        return mActionBar.getNavigationItemCount();
    }

    /**
     * Sets the title.
     *
     * @param title the new title
     * @see com.actionbarsherlock.app.ActionBar#setTitle(java.lang.CharSequence)
     */
    @Override
    public void setTitle(CharSequence title) {
        mActionBar.setTitle(title);
    }

    /**
     * Sets the title.
     *
     * @param resId the new title
     * @see com.actionbarsherlock.app.ActionBar#setTitle(int)
     */
    @Override
    public void setTitle(int resId) {
        mActionBar.setTitle(resId);
    }

    /**
     * Sets the subtitle.
     *
     * @param subtitle the new subtitle
     * @see com.actionbarsherlock.app.ActionBar#setSubtitle(java.lang.CharSequence)
     */
    @Override
    public void setSubtitle(CharSequence subtitle) {
        mActionBar.setSubtitle(subtitle);
    }

    /**
     * Sets the subtitle.
     *
     * @param resId the new subtitle
     * @see com.actionbarsherlock.app.ActionBar#setSubtitle(int)
     */
    @Override
    public void setSubtitle(int resId) {
        mActionBar.setSubtitle(resId);
    }

    /**
     * Sets the display options.
     *
     * @param options the new display options
     * @see com.actionbarsherlock.app.ActionBar#setDisplayOptions(int)
     */
    @Override
    public void setDisplayOptions(int options) {
        mActionBar.setDisplayOptions(options);
    }

    /**
     * Sets the display options.
     *
     * @param options the options
     * @param mask the mask
     * @see com.actionbarsherlock.app.ActionBar#setDisplayOptions(int, int)
     */
    @Override
    public void setDisplayOptions(int options, int mask) {
        mActionBar.setDisplayOptions(options, mask);
    }

    /**
     * Sets the display use logo enabled.
     *
     * @param useLogo the new display use logo enabled
     * @see com.actionbarsherlock.app.ActionBar#setDisplayUseLogoEnabled(boolean)
     */
    @Override
    public void setDisplayUseLogoEnabled(boolean useLogo) {
        mActionBar.setDisplayUseLogoEnabled(useLogo);
    }

    /**
     * Sets the display show home enabled.
     *
     * @param showHome the new display show home enabled
     * @see com.actionbarsherlock.app.ActionBar#setDisplayShowHomeEnabled(boolean)
     */
    @Override
    public void setDisplayShowHomeEnabled(boolean showHome) {
        mActionBar.setDisplayShowHomeEnabled(showHome);
    }

    /**
     * Sets the display home as up enabled.
     *
     * @param showHomeAsUp the new display home as up enabled
     * @see com.actionbarsherlock.app.ActionBar#setDisplayHomeAsUpEnabled(boolean)
     */
    @Override
    public void setDisplayHomeAsUpEnabled(boolean showHomeAsUp) {
        mActionBar.setDisplayHomeAsUpEnabled(showHomeAsUp);
    }

    /**
     * Sets the display show title enabled.
     *
     * @param showTitle the new display show title enabled
     * @see com.actionbarsherlock.app.ActionBar#setDisplayShowTitleEnabled(boolean)
     */
    @Override
    public void setDisplayShowTitleEnabled(boolean showTitle) {
        mActionBar.setDisplayShowTitleEnabled(showTitle);
    }

    /**
     * Sets the display show custom enabled.
     *
     * @param showCustom the new display show custom enabled
     * @see com.actionbarsherlock.app.ActionBar#setDisplayShowCustomEnabled(boolean)
     */
    @Override
    public void setDisplayShowCustomEnabled(boolean showCustom) {
        mActionBar.setDisplayShowCustomEnabled(showCustom);
    }

    /**
     * Sets the background drawable.
     *
     * @param d the new background drawable
     * @see com.actionbarsherlock.app.ActionBar#setBackgroundDrawable(android.graphics.drawable.Drawable)
     */
    @Override
    public void setBackgroundDrawable(Drawable d) {
        mActionBar.setBackgroundDrawable(d);
    }

    /**
     * Sets the stacked background drawable.
     *
     * @param d the new stacked background drawable
     * @see com.actionbarsherlock.app.ActionBar#setStackedBackgroundDrawable(android.graphics.drawable.Drawable)
     */
    @Override
    public void setStackedBackgroundDrawable(Drawable d) {
        mActionBar.setStackedBackgroundDrawable(d);
    }

    /**
     * Sets the split background drawable.
     *
     * @param d the new split background drawable
     * @see com.actionbarsherlock.app.ActionBar#setSplitBackgroundDrawable(android.graphics.drawable.Drawable)
     */
    @Override
    public void setSplitBackgroundDrawable(Drawable d) {
        mActionBar.setSplitBackgroundDrawable(d);
    }

    /**
     * Gets the custom view.
     *
     * @return the custom view
     * @see com.actionbarsherlock.app.ActionBar#getCustomView()
     */
    @Override
    public View getCustomView() {
        return mActionBar.getCustomView();
    }

    /**
     * Gets the title.
     *
     * @return the title
     * @see com.actionbarsherlock.app.ActionBar#getTitle()
     */
    @Override
    public CharSequence getTitle() {
        return mActionBar.getTitle();
    }

    /**
     * Gets the subtitle.
     *
     * @return the subtitle
     * @see com.actionbarsherlock.app.ActionBar#getSubtitle()
     */
    @Override
    public CharSequence getSubtitle() {
        return mActionBar.getSubtitle();
    }

    /**
     * Gets the navigation mode.
     *
     * @return the navigation mode
     * @see com.actionbarsherlock.app.ActionBar#getNavigationMode()
     */
    @Override
    public int getNavigationMode() {
        return mActionBar.getNavigationMode();
    }

    /**
     * Sets the navigation mode.
     *
     * @param mode the new navigation mode
     * @see com.actionbarsherlock.app.ActionBar#setNavigationMode(int)
     */
    @Override
    public void setNavigationMode(int mode) {
        mActionBar.setNavigationMode(mode);
    }

    /**
     * Gets the display options.
     *
     * @return the display options
     * @see com.actionbarsherlock.app.ActionBar#getDisplayOptions()
     */
    @Override
    public int getDisplayOptions() {
        return mActionBar.getDisplayOptions();
    }

    /**
     * The Class TabWrapper.
     */
    public class TabWrapper extends ActionBar.Tab implements android.app.ActionBar.TabListener {
        
        /** The m native tab. */
        final android.app.ActionBar.Tab mNativeTab;
        
        /** The m tag. */
        private Object mTag;
        
        /** The m listener. */
        private TabListener mListener;

        /**
         * Instantiates a new tab wrapper.
         *
         * @param nativeTab the native tab
         */
        public TabWrapper(android.app.ActionBar.Tab nativeTab) {
            mNativeTab = nativeTab;
            mNativeTab.setTag(this);
        }

        /**
         * Gets the position.
         *
         * @return the position
         * @see com.actionbarsherlock.app.ActionBar.Tab#getPosition()
         */
        @Override
        public int getPosition() {
            return mNativeTab.getPosition();
        }

        /**
         * Gets the icon.
         *
         * @return the icon
         * @see com.actionbarsherlock.app.ActionBar.Tab#getIcon()
         */
        @Override
        public Drawable getIcon() {
            return mNativeTab.getIcon();
        }

        /**
         * Gets the text.
         *
         * @return the text
         * @see com.actionbarsherlock.app.ActionBar.Tab#getText()
         */
        @Override
        public CharSequence getText() {
            return mNativeTab.getText();
        }

        /**
         * Sets the icon.
         *
         * @param icon the icon
         * @return the tab
         * @see com.actionbarsherlock.app.ActionBar.Tab#setIcon(android.graphics.drawable.Drawable)
         */
        @Override
        public Tab setIcon(Drawable icon) {
            mNativeTab.setIcon(icon);
            return this;
        }

        /**
         * Sets the icon.
         *
         * @param resId the res id
         * @return the tab
         * @see com.actionbarsherlock.app.ActionBar.Tab#setIcon(int)
         */
        @Override
        public Tab setIcon(int resId) {
            mNativeTab.setIcon(resId);
            return this;
        }

        /**
         * Sets the text.
         *
         * @param text the text
         * @return the tab
         * @see com.actionbarsherlock.app.ActionBar.Tab#setText(java.lang.CharSequence)
         */
        @Override
        public Tab setText(CharSequence text) {
            mNativeTab.setText(text);
            return this;
        }

        /**
         * Sets the text.
         *
         * @param resId the res id
         * @return the tab
         * @see com.actionbarsherlock.app.ActionBar.Tab#setText(int)
         */
        @Override
        public Tab setText(int resId) {
            mNativeTab.setText(resId);
            return this;
        }

        /**
         * Sets the custom view.
         *
         * @param view the view
         * @return the tab
         * @see com.actionbarsherlock.app.ActionBar.Tab#setCustomView(android.view.View)
         */
        @Override
        public Tab setCustomView(View view) {
            mNativeTab.setCustomView(view);
            return this;
        }

        /**
         * Sets the custom view.
         *
         * @param layoutResId the layout res id
         * @return the tab
         * @see com.actionbarsherlock.app.ActionBar.Tab#setCustomView(int)
         */
        @Override
        public Tab setCustomView(int layoutResId) {
            mNativeTab.setCustomView(layoutResId);
            return this;
        }

        /**
         * Gets the custom view.
         *
         * @return the custom view
         * @see com.actionbarsherlock.app.ActionBar.Tab#getCustomView()
         */
        @Override
        public View getCustomView() {
            return mNativeTab.getCustomView();
        }

        /**
         * Sets the tag.
         *
         * @param obj the obj
         * @return the tab
         * @see com.actionbarsherlock.app.ActionBar.Tab#setTag(java.lang.Object)
         */
        @Override
        public Tab setTag(Object obj) {
            mTag = obj;
            return this;
        }

        /**
         * Gets the tag.
         *
         * @return the tag
         * @see com.actionbarsherlock.app.ActionBar.Tab#getTag()
         */
        @Override
        public Object getTag() {
            return mTag;
        }

        /**
         * Sets the tab listener.
         *
         * @param listener the listener
         * @return the tab
         * @see com.actionbarsherlock.app.ActionBar.Tab#setTabListener(com.actionbarsherlock.app.ActionBar.TabListener)
         */
        @Override
        public Tab setTabListener(TabListener listener) {
            mNativeTab.setTabListener(listener != null ? this : null);
            mListener = listener;
            return this;
        }

        /**
         * Select.
         *
         * @see com.actionbarsherlock.app.ActionBar.Tab#select()
         */
        @Override
        public void select() {
            mNativeTab.select();
        }

        /**
         * Sets the content description.
         *
         * @param resId the res id
         * @return the tab
         * @see com.actionbarsherlock.app.ActionBar.Tab#setContentDescription(int)
         */
        @Override
        public Tab setContentDescription(int resId) {
            mNativeTab.setContentDescription(resId);
            return this;
        }

        /**
         * Sets the content description.
         *
         * @param contentDesc the content desc
         * @return the tab
         * @see com.actionbarsherlock.app.ActionBar.Tab#setContentDescription(java.lang.CharSequence)
         */
        @Override
        public Tab setContentDescription(CharSequence contentDesc) {
            mNativeTab.setContentDescription(contentDesc);
            return this;
        }

        /**
         * Gets the content description.
         *
         * @return the content description
         * @see com.actionbarsherlock.app.ActionBar.Tab#getContentDescription()
         */
        @Override
        public CharSequence getContentDescription() {
            return mNativeTab.getContentDescription();
        }

        /**
         * On tab reselected.
         *
         * @param tab the tab
         * @param ft the ft
         * @see android.app.ActionBar.TabListener#onTabReselected(android.app.ActionBar.Tab, android.app.FragmentTransaction)
         */
        @Override
        public void onTabReselected(android.app.ActionBar.Tab tab, android.app.FragmentTransaction ft) {
            if (mListener != null) {
                FragmentTransaction trans = null;
                if (mActivity instanceof SherlockFragmentActivity) {
                    trans = ((SherlockFragmentActivity)mActivity).getSupportFragmentManager().beginTransaction()
                            .disallowAddToBackStack();
                }

                mListener.onTabReselected(this, trans);

                if (trans != null && !trans.isEmpty()) {
                    trans.commit();
                }
            }
        }

        /**
         * On tab selected.
         *
         * @param tab the tab
         * @param ft the ft
         * @see android.app.ActionBar.TabListener#onTabSelected(android.app.ActionBar.Tab, android.app.FragmentTransaction)
         */
        @Override
        public void onTabSelected(android.app.ActionBar.Tab tab, android.app.FragmentTransaction ft) {
            if (mListener != null) {

                if (mFragmentTransaction == null && mActivity instanceof SherlockFragmentActivity) {
                    mFragmentTransaction = ((SherlockFragmentActivity)mActivity).getSupportFragmentManager().beginTransaction()
                            .disallowAddToBackStack();
                }

                mListener.onTabSelected(this, mFragmentTransaction);

                if (mFragmentTransaction != null) {
                    if (!mFragmentTransaction.isEmpty()) {
                        mFragmentTransaction.commit();
                    }
                    mFragmentTransaction = null;
                }
            }
        }

        /**
         * On tab unselected.
         *
         * @param tab the tab
         * @param ft the ft
         * @see android.app.ActionBar.TabListener#onTabUnselected(android.app.ActionBar.Tab, android.app.FragmentTransaction)
         */
        @Override
        public void onTabUnselected(android.app.ActionBar.Tab tab, android.app.FragmentTransaction ft) {
            if (mListener != null) {
                FragmentTransaction trans = null;
                if (mActivity instanceof SherlockFragmentActivity) {
                    trans = ((SherlockFragmentActivity)mActivity).getSupportFragmentManager().beginTransaction()
                            .disallowAddToBackStack();
                    mFragmentTransaction = trans;
                }

                mListener.onTabUnselected(this, trans);
            }
        }
    }

    /**
     * New tab.
     *
     * @return the tab
     * @see com.actionbarsherlock.app.ActionBar#newTab()
     */
    @Override
    public Tab newTab() {
        return new TabWrapper(mActionBar.newTab());
    }

    /**
     * Adds the tab.
     *
     * @param tab the tab
     * @see com.actionbarsherlock.app.ActionBar#addTab(com.actionbarsherlock.app.ActionBar.Tab)
     */
    @Override
    public void addTab(Tab tab) {
        mActionBar.addTab(((TabWrapper)tab).mNativeTab);
    }

    /**
     * Adds the tab.
     *
     * @param tab the tab
     * @param setSelected the set selected
     * @see com.actionbarsherlock.app.ActionBar#addTab(com.actionbarsherlock.app.ActionBar.Tab, boolean)
     */
    @Override
    public void addTab(Tab tab, boolean setSelected) {
        mActionBar.addTab(((TabWrapper)tab).mNativeTab, setSelected);
    }

    /**
     * Adds the tab.
     *
     * @param tab the tab
     * @param position the position
     * @see com.actionbarsherlock.app.ActionBar#addTab(com.actionbarsherlock.app.ActionBar.Tab, int)
     */
    @Override
    public void addTab(Tab tab, int position) {
        mActionBar.addTab(((TabWrapper)tab).mNativeTab, position);
    }

    /**
     * Adds the tab.
     *
     * @param tab the tab
     * @param position the position
     * @param setSelected the set selected
     * @see com.actionbarsherlock.app.ActionBar#addTab(com.actionbarsherlock.app.ActionBar.Tab, int, boolean)
     */
    @Override
    public void addTab(Tab tab, int position, boolean setSelected) {
        mActionBar.addTab(((TabWrapper)tab).mNativeTab, position, setSelected);
    }

    /**
     * Removes the tab.
     *
     * @param tab the tab
     * @see com.actionbarsherlock.app.ActionBar#removeTab(com.actionbarsherlock.app.ActionBar.Tab)
     */
    @Override
    public void removeTab(Tab tab) {
        mActionBar.removeTab(((TabWrapper)tab).mNativeTab);
    }

    /**
     * Removes the tab at.
     *
     * @param position the position
     * @see com.actionbarsherlock.app.ActionBar#removeTabAt(int)
     */
    @Override
    public void removeTabAt(int position) {
        mActionBar.removeTabAt(position);
    }

    /**
     * Removes the all tabs.
     *
     * @see com.actionbarsherlock.app.ActionBar#removeAllTabs()
     */
    @Override
    public void removeAllTabs() {
        mActionBar.removeAllTabs();
    }

    /**
     * Select tab.
     *
     * @param tab the tab
     * @see com.actionbarsherlock.app.ActionBar#selectTab(com.actionbarsherlock.app.ActionBar.Tab)
     */
    @Override
    public void selectTab(Tab tab) {
        mActionBar.selectTab(((TabWrapper)tab).mNativeTab);
    }

    /**
     * Gets the selected tab.
     *
     * @return the selected tab
     * @see com.actionbarsherlock.app.ActionBar#getSelectedTab()
     */
    @Override
    public Tab getSelectedTab() {
        android.app.ActionBar.Tab selected = mActionBar.getSelectedTab();
        return (selected != null) ? (Tab)selected.getTag() : null;
    }

    /**
     * Gets the tab at.
     *
     * @param index the index
     * @return the tab at
     * @see com.actionbarsherlock.app.ActionBar#getTabAt(int)
     */
    @Override
    public Tab getTabAt(int index) {
        android.app.ActionBar.Tab selected = mActionBar.getTabAt(index);
        return (selected != null) ? (Tab)selected.getTag() : null;
    }

    /**
     * Gets the tab count.
     *
     * @return the tab count
     * @see com.actionbarsherlock.app.ActionBar#getTabCount()
     */
    @Override
    public int getTabCount() {
        return mActionBar.getTabCount();
    }

    /**
     * Gets the height.
     *
     * @return the height
     * @see com.actionbarsherlock.app.ActionBar#getHeight()
     */
    @Override
    public int getHeight() {
        return mActionBar.getHeight();
    }

    /**
     * Show.
     *
     * @see com.actionbarsherlock.app.ActionBar#show()
     */
    @Override
    public void show() {
        mActionBar.show();
    }

    /**
     * Hide.
     *
     * @see com.actionbarsherlock.app.ActionBar#hide()
     */
    @Override
    public void hide() {
        mActionBar.hide();
    }

    /**
     * Checks if is showing.
     *
     * @return true, if is showing
     * @see com.actionbarsherlock.app.ActionBar#isShowing()
     */
    @Override
    public boolean isShowing() {
        return mActionBar.isShowing();
    }

    /**
     * Adds the on menu visibility listener.
     *
     * @param listener the listener
     * @see com.actionbarsherlock.app.ActionBar#addOnMenuVisibilityListener(com.actionbarsherlock.app.ActionBar.OnMenuVisibilityListener)
     */
    @Override
    public void addOnMenuVisibilityListener(OnMenuVisibilityListener listener) {
        mMenuVisibilityListeners.add(listener);
    }

    /**
     * Removes the on menu visibility listener.
     *
     * @param listener the listener
     * @see com.actionbarsherlock.app.ActionBar#removeOnMenuVisibilityListener(com.actionbarsherlock.app.ActionBar.OnMenuVisibilityListener)
     */
    @Override
    public void removeOnMenuVisibilityListener(OnMenuVisibilityListener listener) {
        mMenuVisibilityListeners.remove(listener);
    }

    /**
     * On menu visibility changed.
     *
     * @param isVisible the is visible
     * @see android.app.ActionBar.OnMenuVisibilityListener#onMenuVisibilityChanged(boolean)
     */
    @Override
    public void onMenuVisibilityChanged(boolean isVisible) {
        for (OnMenuVisibilityListener listener : mMenuVisibilityListeners) {
            listener.onMenuVisibilityChanged(isVisible);
        }
    }
}
