package com.webileapps.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import org.bitbucket.fredgrott.gwsamulet.R;

// TODO: Auto-generated Javadoc
/**
 * The Class SegmentedControl.
 */
public class SegmentedControl extends RadioGroup {
	
//	private static final String TAG = "SegmentedControl";

	/**
 * Instantiates a new segmented control.
 *
 * @param context the context
 * @param attrs the attrs
 */
public SegmentedControl(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	/**
	 * Instantiates a new segmented control.
	 *
	 * @param context the context
	 */
	public SegmentedControl(Context context) {
		super(context);
		init();
	}
    
    /** The m pass through listener. */
    private PassThroughHierarchyChangeListener mPassThroughListener;
	
	/**
	 * Inits the.
	 */
	private void init() {
		this.mChildOnCheckedChangeListener = new CheckedStateTracker();
        mPassThroughListener = new PassThroughHierarchyChangeListener();
        super.setOnHierarchyChangeListener(mPassThroughListener);
	}
	
    /**
     * The listener interface for receiving passThroughHierarchyChange events.
     * The class that is interested in processing a passThroughHierarchyChange
     * event implements this interface, and the object created
     * with that class is registered with a component using the
     * component's <code>addPassThroughHierarchyChangeListener<code> method. When
     * the passThroughHierarchyChange event occurs, that object's appropriate
     * method is invoked.
     *
     * @see PassThroughHierarchyChangeEvent
     */
    private class PassThroughHierarchyChangeListener implements
    ViewGroup.OnHierarchyChangeListener {

		/**
		 * {@inheritDoc}
		 */
		public void onChildViewAdded(View parent, View child) {
			if (parent == SegmentedControl.this && child instanceof RadioButton) {
				((RadioButton) child).setOnCheckedChangeListener(mChildOnCheckedChangeListener);
				redrawChildrenBackgrounds();
			}
		}

		/**
		 * {@inheritDoc}
		 */
		public void onChildViewRemoved(View parent, View child) {
			if (parent == SegmentedControl.this && child instanceof RadioButton) {
				((RadioButton) child).setOnCheckedChangeListener(null);
				redrawChildrenBackgrounds();
			}
		}
	}
    
    /**
     * Redraw children backgrounds.
     */
    private void redrawChildrenBackgrounds() {
    	int count = SegmentedControl.this.getChildCount();
    	
		if(count == 0) return;
		if(count == 1) {
			View childView = SegmentedControl.this.getChildAt(0);
			boolean isChecked = ((RadioButton)childView).isChecked();
			if(isChecked)
				childView.setBackgroundResource(R.drawable.single_selected);
			else
				childView.setBackgroundResource(R.drawable.single_selected);
			return;
		}
		for(int position = 0; position< count; position++) {
			View childView = SegmentedControl.this.getChildAt(position);
			boolean isChecked = ((RadioButton)childView).isChecked();
			if(position == 0) {
				if(isChecked)
					childView.setBackgroundResource(R.drawable.left_selected);
				else
					childView.setBackgroundResource(R.drawable.left_unselected);
			} else if (position == count-1) {
				if(isChecked)
					childView.setBackgroundResource(R.drawable.right_selected);
				else
					childView.setBackgroundResource(R.drawable.right_unselected);
			} else {
				if(isChecked)
					childView.setBackgroundResource(R.drawable.middle_selected);
				else
					childView.setBackgroundResource(R.drawable.middle_unselected);
			}
		}
    }

    /** The m child on checked change listener. */
    private CheckedStateTracker mChildOnCheckedChangeListener;
    
    /**
     * The Class CheckedStateTracker.
     */
    private class CheckedStateTracker implements CompoundButton.OnCheckedChangeListener {
    	
	    /**
    	 * On checked changed.
    	 *
    	 * @param buttonView the button view
    	 * @param isChecked the is checked
    	 * @see android.widget.CompoundButton.OnCheckedChangeListener#onCheckedChanged(android.widget.CompoundButton, boolean)
    	 */
	    @Override
    	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			redrawChildrenBackgrounds();
    	}
    }
}