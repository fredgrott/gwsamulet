package com.throrinstudio.android.library.widgets.dashboard;

import java.util.ArrayList;

import org.bitbucket.fredgrott.gwsamulet.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

// TODO: Auto-generated Javadoc
/**
 * The Class DashBoardAdapter.
 */
public class DashBoardAdapter extends BaseAdapter{

	
	/** The m inflater. */
	private LayoutInflater 				mInflater;
	
	/** The m context. */
	private Context						mContext;
	
	/** The m elements. */
	private ArrayList<DashBoardElement> mElements;
	
	/**
	 * Instantiates a new dash board adapter.
	 *
	 * @param c the c
	 */
	public DashBoardAdapter(Context c){
		mContext 	= c;
		mInflater	= (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mElements	= new ArrayList<DashBoardElement>();
	}
	
	/**
	 * Adds the element.
	 *
	 * @param img the img
	 * @param libelle the libelle
	 * @param listener the listener
	 */
	public void addElement(Object img, String libelle, DashBoardElement.OnClickListener listener){
		DashBoardElement element = new DashBoardElement(img, libelle, listener);
		mElements.add(element);
		this.notifyDataSetInvalidated();
	}
	
	/**
	 * Adds the element.
	 *
	 * @param element the element
	 */
	public void addElement(DashBoardElement element){
		mElements.add(element);
		this.notifyDataSetInvalidated();
	}
	
	
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		return mElements.size();
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public DashBoardElement getItem(int arg0) {
		return mElements.get(arg0);
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		final ViewHandler holder;
		
		if(convertView == null) {
            vi 				= mInflater.inflate(R.layout.widget_dashboard_item, null);
            holder			= new ViewHandler();
            holder.image	= (ImageView) vi.findViewById(R.id.dashboard_item_img);
            holder.text 	= (TextView) vi.findViewById(R.id.dashboard_item_name);
            vi.setTag(holder);
        } else {
        	holder = (ViewHandler) vi.getTag();
        }
		
		DashBoardElement element = getItem(position);
		
		if(element.getRes() instanceof Integer){
			holder.image.setImageResource((Integer) element.getRes());
		}else if(element.getRes() instanceof Drawable){
			holder.image.setImageDrawable((Drawable) element.getRes());
		}else if(element.getRes() instanceof Bitmap){
			holder.image.setImageBitmap((Bitmap) element.getRes());
		}
		
		holder.text.setText(element.getLibelle());
		
		return vi;
	}

	
	/**
	 * The Class ViewHandler.
	 */
	public static class ViewHandler{
		
		/** The image. */
		public ImageView 	image;
		
		/** The text. */
		public TextView 	text;
	}
}
