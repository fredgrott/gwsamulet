package com.throrinstudio.android.library.widgets.dashboard;

import org.bitbucket.fredgrott.gwsamulet.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;

// TODO: Auto-generated Javadoc
/**
 * The Class DashBoardLayout.
 */
public class DashBoardLayout extends LinearLayout{

	/** The m inflater. */
	private LayoutInflater 				mInflater;
	
	/** The m grid dashboard. */
	private GridView					mGridDashboard;
	
	/** The m content view. */
	private	LinearLayout				mContentView;


	/**
	 * Instantiates a new dash board layout.
	 *
	 * @param context the context
	 * @param attrs the attrs
	 */
	public DashBoardLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		mInflater 		= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mContentView 	= (LinearLayout) mInflater.inflate(R.layout.widget_dashboard, null);
		mGridDashboard	= (GridView) mContentView.findViewById(R.id.dashboard_grid);
		
		mGridDashboard.setAdapter(new DashBoardAdapter(context));
		mGridDashboard.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
				DashBoardElement element = (DashBoardElement) mGridDashboard.getAdapter().getItem(pos);
				element.getListener().onClick(arg1);
			}
		});
		
		this.addView(mContentView);
	}

	/**
	 * Adds the element.
	 *
	 * @param img the img
	 * @param libelle the libelle
	 * @param listener the listener
	 */
	public void addElement(Object img, String libelle, DashBoardElement.OnClickListener listener){
		((DashBoardAdapter)mGridDashboard.getAdapter()).addElement(img, libelle, listener);
	}
	
	/**
	 * Adds the element.
	 *
	 * @param element the element
	 */
	public void addElement(DashBoardElement element){
		((DashBoardAdapter)mGridDashboard.getAdapter()).addElement(element);
	}
	
	
}
