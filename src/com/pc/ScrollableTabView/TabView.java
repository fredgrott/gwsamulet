package com.pc.ScrollableTabView;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Pietro Caselani
 */
public class TabView extends TextView {

    public TabView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TabView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}