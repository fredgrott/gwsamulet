package com.standalonecode.bubbleview;

import org.bitbucket.fredgrott.gwsamulet.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.widget.LinearLayout;


// TODO: Auto-generated Javadoc
/**
 * The Class BubbleView.
 */
public class BubbleView extends LinearLayout
{
    
    /** The Constant DEFAULT_POINTER_HEIGHT. */
    private static final float DEFAULT_POINTER_HEIGHT = 25f;
    
    /** The Constant DEFAULT_POINTER_WIDTH. */
    private static final float DEFAULT_POINTER_WIDTH = 50f;
    
    /** The Constant DEFAULT_CORNER_RADIUS. */
    private static final float DEFAULT_CORNER_RADIUS = 15f;
    
    /** The Constant DEFAULT_BUBBLE_COLOR. */
    private static final int DEFAULT_BUBBLE_COLOR = Color.GRAY;
    
    /** The Constant DEFAULT_BUBBLE_OUTLINE_COLOR. */
    private static final int DEFAULT_BUBBLE_OUTLINE_COLOR = 0xffb7bcbc;
    
    /** The Constant DEFAULT_SHADOW_COLOR. */
    private static final int DEFAULT_SHADOW_COLOR = Color.GRAY;
    
    /** The Constant DEFAULT_SHADOW_RADIUS. */
    private static final float DEFAULT_SHADOW_RADIUS = 5f;
    
    /** The Constant DEFAULT_SHADOW_OFFSET_X. */
    private static final float DEFAULT_SHADOW_OFFSET_X = 5f;
    
    /** The Constant DEFAULT_SHADOW_OFFSET_Y. */
    private static final float DEFAULT_SHADOW_OFFSET_Y = 5f;
    
    /** The Constant DEFAULT_BUBBLE_GRADIENT_TOP_COLOR. */
    private static final int DEFAULT_BUBBLE_GRADIENT_TOP_COLOR = Color.WHITE;
    
    /** The Constant DEFAULT_BUBBLE_GRADIENT_BOTTOM_COLOR. */
    private static final int DEFAULT_BUBBLE_GRADIENT_BOTTOM_COLOR = 0xffb7bcbc;

    /** The bubble path. */
    private Path bubblePath;
    
    /** The pointer height. */
    private float pointerHeight = DEFAULT_POINTER_HEIGHT;
    
    /** The pointer width. */
    private float pointerWidth = DEFAULT_POINTER_WIDTH;
    
    /** The corner radius. */
    private float cornerRadius = DEFAULT_CORNER_RADIUS;
    
    /** The bubble color. */
    private int bubbleColor = DEFAULT_BUBBLE_COLOR;
    
    /** The bubble outline color. */
    private int bubbleOutlineColor = DEFAULT_BUBBLE_OUTLINE_COLOR;
    
    /** The shadow color. */
    @SuppressWarnings("unused")
	private int shadowColor = DEFAULT_SHADOW_COLOR;
    
    /** The bubble path paint. */
    private Paint bubblePathPaint;
    
    /** The bubble path outline paint. */
    private Paint bubblePathOutlinePaint;
    
    /** The bubble gradient top color. */
    private int bubbleGradientTopColor = DEFAULT_BUBBLE_GRADIENT_TOP_COLOR;
    
    /** The bubble gradient bottom color. */
    private int bubbleGradientBottomColor = DEFAULT_BUBBLE_GRADIENT_BOTTOM_COLOR;
    
    /** The shadow radius. */
    @SuppressWarnings("unused")
	private float shadowRadius = DEFAULT_SHADOW_RADIUS;
    
    /** The shadow offset x. */
    @SuppressWarnings("unused")
	private float shadowOffsetX = DEFAULT_SHADOW_OFFSET_X;
    
    /** The shadow offset y. */
    @SuppressWarnings("unused")
	private float shadowOffsetY = DEFAULT_SHADOW_OFFSET_Y;

    /**
     * Instantiates a new bubble view.
     *
     * @param context the context
     */
    public BubbleView(Context context)
    {
        super(context);
        initPaint();
    }

    /**
     * Instantiates a new bubble view.
     *
     * @param context the context
     * @param attrs the attrs
     */
    public BubbleView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        initAttributes(context, attrs);
        initPaint();
    }

    /* (non-Javadoc)
     * @see android.widget.LinearLayout#onLayout(boolean, int, int, int, int)
     */
    @Override
    protected void onLayout (boolean changed, int l, int t, int r, int b)
    {
        super.onLayout(changed, l, t, r, b);
        this.bubblePath = calculateBubblePath(l, t, r, b);
        // The following was for a gradient inside the bubble that currently isn't necessary.
        // However, it was causing the background color not to be drawn at all when padding and/or margin
        // was specified.
//        bubblePathPaint.setShader(calculateGradient(l, t, r, b));
    }

    /* (non-Javadoc)
     * @see android.widget.LinearLayout#onDraw(android.graphics.Canvas)
     */
    @Override
    protected void onDraw (Canvas canvas)
    {
        super.onDraw(canvas);
        canvas.drawPath(bubblePath, bubblePathPaint);
        canvas.drawPath(bubblePath, bubblePathOutlinePaint);
    }

    /**
     * Inits the attributes.
     *
     * @param context the context
     * @param attrs the attrs
     */
    private void initAttributes(Context context, AttributeSet attrs)
    {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.BubbleView);
        this.pointerHeight = a.getFloat(R.styleable.BubbleView_bubble_pointerHeight, DEFAULT_POINTER_HEIGHT);
        this.pointerWidth = a.getFloat(R.styleable.BubbleView_bubble_pointerWidth, DEFAULT_POINTER_WIDTH);
        this.cornerRadius = a.getFloat(R.styleable.BubbleView_bubble_cornerRadius, DEFAULT_CORNER_RADIUS);
        this.bubbleColor = a.getInt(R.styleable.BubbleView_bubble_color, DEFAULT_BUBBLE_COLOR);
        this.bubbleOutlineColor = a.getInt(R.styleable.BubbleView_bubble_outlineColor, DEFAULT_BUBBLE_OUTLINE_COLOR);
        this.shadowColor = a.getInt(R.styleable.BubbleView_bubble_shadowColor, DEFAULT_SHADOW_COLOR);
        this.shadowRadius = a.getFloat(R.styleable.BubbleView_bubble_shadowRadius, DEFAULT_SHADOW_RADIUS);
        this.shadowOffsetX = a.getFloat(R.styleable.BubbleView_bubble_shadowOffsetX, DEFAULT_SHADOW_OFFSET_X);
        this.shadowOffsetY = a.getFloat(R.styleable.BubbleView_bubble_shadowOffsetY, DEFAULT_SHADOW_OFFSET_Y);
        this.bubbleGradientTopColor = a.getInt(R.styleable.BubbleView_bubble_gradientTopColor, DEFAULT_BUBBLE_GRADIENT_TOP_COLOR);
        this.bubbleGradientBottomColor = a.getInt(R.styleable.BubbleView_bubble_gradientBottomColor, DEFAULT_BUBBLE_GRADIENT_BOTTOM_COLOR);
    }

    /**
     * Inits the paint.
     */
    private void initPaint()
    {
        // Setup Paint for filling the path.
        this.bubblePathPaint = new Paint();
        bubblePathPaint.setStyle(Paint.Style.FILL);
        bubblePathPaint.setColor(bubbleColor);
//        bubblePathPaint.setShadowLayer(shadowRadius, shadowOffsetX, shadowOffsetY, shadowColor);

        // Setup Paint for stroking the path.
        this.bubblePathOutlinePaint = new Paint();
        bubblePathOutlinePaint.setStyle(Paint.Style.STROKE);
        bubblePathOutlinePaint.setColor(bubbleOutlineColor);
        bubblePathOutlinePaint.setStrokeWidth(2.0f);
    }

    /**
     * Calculate bubble path.
     *
     * @param l the l
     * @param t the t
     * @param r the r
     * @param b the b
     * @return the path
     */
    private Path calculateBubblePath(int l, int t, int r, int b)
    {
        Path path = new Path();

        float viewBottom = b - t;
        float bubbleBottom = viewBottom - pointerHeight;
        float viewWidth = r - l;
        float viewCenterX = viewWidth / 2.0f;
        float halfPointerWidth = pointerWidth / 2.0f;
        RectF cornerRect = new RectF(0, 0, cornerRadius, cornerRadius);

        // If there is a practical pointer, then draw the bubble and pointer.
        if ((pointerHeight > 0f) && (pointerWidth > 0f))
        {
          // Start at the point of the triangle at the bottom and draw the line up to the bottom of the round rect.
          path.moveTo(viewCenterX, viewBottom);
          path.lineTo(viewCenterX + halfPointerWidth, bubbleBottom);

          // Move to the bottom right corner of the bubble rect and draw the corner.
          path.lineTo(viewWidth - cornerRadius, bubbleBottom);
          cornerRect.offsetTo(viewWidth - cornerRadius, bubbleBottom - cornerRadius);
          path.arcTo(cornerRect, 90f, -90f);

          // Move up to the top right corner and draw the corner.
          path.lineTo(viewWidth, cornerRadius);
          cornerRect.offsetTo(viewWidth - cornerRadius, 0);
          path.arcTo(cornerRect, 0f, -90f);

          // Move up to the top left corner and draw the corner.
          path.lineTo(cornerRadius, 0);
          cornerRect.offsetTo(0, 0);
          path.arcTo(cornerRect, 270f, -90f);

          // Move up to the bottom left corner and draw the corner.
          path.lineTo(0, bubbleBottom - cornerRadius);
          cornerRect.offsetTo(0, bubbleBottom - cornerRadius);
          path.arcTo(cornerRect, 180f, -90f);

          // Finish the bottom and the left hand side of the pointer.
          path.lineTo(viewCenterX - halfPointerWidth, bubbleBottom);
          path.lineTo(viewCenterX, viewBottom);
          path.close();
        }

        // No pointer.  Just make a round rect for the bubble.
        else
        {
          path.addRoundRect(new RectF(0, 0, viewWidth, viewBottom), cornerRadius, cornerRadius, Path.Direction.CW);
        }

        return path;
    }

    /**
     * Calculate gradient.
     *
     * @param l the l
     * @param t the t
     * @param r the r
     * @param b the b
     * @return the linear gradient
     */
    @SuppressWarnings("unused")
	private LinearGradient calculateGradient(int l, int t, int r, int b)
    {
      return new LinearGradient(0, 0, 0, b - t, bubbleGradientTopColor, bubbleGradientBottomColor, Shader.TileMode.CLAMP);
    }

}
