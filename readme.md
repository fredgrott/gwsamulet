GWSAmulet
---


# License

# Credits

## From Jake Wharton

### ActionBarSherlock

[ActionBarSherlock]()

### NineOldAndroids

[NineOldAndroids]()

### Hansel And Gretel

[HanselAndGretel]()

### ActivityCompat2

[ActivityCompat2]()

### NotificationCompat2

[NotificationCompat2]()

### SwipeToDismiss

[SwipeToDismiss]()

### ViewPagerIndicator

[ViewPAgerIndicator]()



## From SimonVT

### AndroidCalendarView

[AndroidCalendarView]()

### AndroidDatePicker

[AndroidDatePicker]()

### AndroidNumberPicker

[AndroidNumberPicker]()

### AndroidTimePicker

[AndroidTimePicker]()


##

### RoboGuiceSherlock

[RoboGuiceSherlock]()

## From

### GridLayout

[GridLayout]()

### ViewPagerExtensions

[ViewPagerExtensions]()

### ICS Property

[ICSProperty]()


## From

### ShowcaseView

[ShowcaseView]()

## From

### SegmentedControl

[SegmentedControl]()

## From

### DockableView

[DockableView]()

## From

### GraphView

[GraphView]()


## From

### BubbleView

[BubbleView]()

## From

### Grist

[Grist]()

## From

### FlipView

[FlipView]()

## From

### AutofitTextView

[AutofitTextView]()

## From

AutoBgButton

[AutoBgButton]()


## From

### Seekbar

[SeekBar]()


## From

### Wheel Widget

[Wheel Widget]()

## From

### RangeSeekBar

[RangeSeekBar]()

## From

### Dashboard Library

[Dashboard Library]()

## From

### FormEditText

[FormEditText]()

## From

### HelpViewPager

[HelpViewPager]()

## From

### UITableView

[UITableView]()

## From Square

### Seismic

[Seismic]()

### Otto

[Otto]()

## From

### ViewPager3D

[ViewPager3D]()

## From

### NewQuickAction

[NewQuickAction]()

## From

### TouchLib

[TouchLib]()

## From

### RetractiveScrollViews

[RetractiveScrollViews]()

## From

### New Popup Menu

[NewPopUp Menu]()

## From

### Android Expandable Layout

[AndroidExpandableLayout]()

## From

### Android Flow Layout

[AndroidFlowLayout]()


## From

### DeselectableRadioButton

[DeselectableRadioButton]()


## From

### SplitView

[SplitView]()

## From

### Android Model Binder

[AndroidModelBinder]()

## From

### Android Collapsible Search Meun

[AndroidCollapsibleSearchMenu]()

## From

### GroupedTextViews

[GroupedTextViews]()

## From Tom Barrasso

### GridFragment

[GridFragment]()

## From

### ScrollTabs

[ScrollTabs]()

## From

### SatelliteMenu

[SatelliteMenu]()

## From

### Android Menu Drawer

[AndroidMenuDrawer]()

## From

### Shared Drag and Drop

[SharedDragAndDrop]()

## From

### LibSlideMenu

[LibSlideMenu]()

## From

### StickyScrollView

[StickyScrollView]()

### StickyListHeaders

[StickyListHeaders]()

## From

### AndroidUndergarment

[AndroidUndergarment]()

## From

### Android LightBox

[AndroidLightBox]()

## From

### AndroidGestureListView

[AndroidGestureListView]()

## From

### AndroidPullToRefresh

[AndroidPullToRefresh]()

## From

### Crouton

[Crouton]()

## From

### SynchronizedScrolling

[SynchronizedScrolling]()


## From

### ViewPagerTabs

[ViewPagerTabs]()

## From

### AndroidSwitchBackport

[AndroidSwitchBackport]()


## From

### RibbonMenu

[RibbonMenu]()



